# JSP Assignment 1

Implements a login form with email and password fields, where the initial values for both are defined using declarative tags, contained in the `login.jsp` file.

The form submissions are then validated against a single preset in the `validateLogin.jsp` page.

If the input is invalid or null, the user is redirected to the `error.jsp` page. Otherwise a welcome message is shown.

The `displayDatabaseTable.jsp` file contains instructions to connect to a database using JDBC to display a specified table without predefining the column names.