<%@page import="java.sql.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<html>
    <head>
        <title>Arbitrary Fetch</title>
    </head>
    <body>

    <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
    url="jdbc:mysql://localhost:3306/test"
    user="sys as sysdba"  password="Oracle_1"/>

    <sql:query dataSource="${snapshot}" var="result">
    SELECT * FROM someTable;
    </sql:query>

    <table border="1">
    <thead>
        <tr>
        <c:forEach var="columnName" items="${result.columnNames}">
            <th>${columnName}</th>
        </c:forEach>
        </tr>
    </thead>
    
    <tbody>
        <c:forEach var="row" items="${result.rows}">
        <tr>
            <c:forEach var="columnValue" items="${row}">
            <td>${columnValue}</td>
            </c:forEach>
        </tr>
        </c:forEach>
    </tbody>
    </table>

    </body>
</html>
