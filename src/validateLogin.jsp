<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Validate Login</title>
</head>
<body>
<%
	String email = request.getParameter("email");
	String password = request.getParameter("password");

	if (email == null || password == null) {
		response.sendRedirect("error.jsp");
		return;
	}

	if (!email.equals("abc@xyz.com") || !password.equals("defghi")) {
		response.sendRedirect("error.jsp");
		return;
	}
%>
	<h1>Welcome!</h1>
	<p>You have successfully logged in.</p>
</body>
</html>
